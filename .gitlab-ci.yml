# Copyright 2020-2024, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

# Detached MR pipelines, needed due to https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/438

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_PIPELINE_SOURCE == 'push'

stages:
  - container_prep
  - build
  - deploy

include: ".gitlab-ci/containers.yml"

# Check reuse.software compliance
reuse:
  stage: build
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - git clean -f
    - reuse lint

# Do the build
android:build:
  stage: build
  extends: .monado.image.debian:android-sdk
  script:
    # Be absolutely sure the official artifacts are not here before building this non-release build
    - rm -rf installable_runtime_broker/src/official
    - ./gradlew clean
    - ./gradlew assembleDebug
    - ./gradlew spotlessCheck
    - ./gradlew assembleRelease
    - cd installable_runtime_broker/ && ../gradlew bundle
  artifacts:
   paths:
     - sampleclient/build/outputs/apk/debug/sampleclient-debug.apk
     - installable_runtime_broker/build/outputs/apk/dev/debug/*
     - installable_runtime_broker/build/outputs/apk/dev/release/*
     - installable_runtime_broker/build/outputs/mapping/devRelease/*

# Do the build for releases
release:build:
  stage: deploy
  extends: .monado.image.debian:android-sdk
  environment:
    name: staging
  # only run this on tags
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "monado/utilities" && $CI_COMMIT_TAG =~ /^release-[.0-9]+.*$/'
  script:
    - .gitlab-ci/get-official-assets.sh
    - ./build-release-artifacts.sh
    - rm -rf installable_runtime_broker/src/official
  artifacts:
   paths:
     - installable_runtime_broker/build/outputs/apk/official/release/*
     - installable_runtime_broker/build/outputs/mapping/officialRelease/*
     - installable_runtime_broker/build/outputs/bundle/officialRelease/*
