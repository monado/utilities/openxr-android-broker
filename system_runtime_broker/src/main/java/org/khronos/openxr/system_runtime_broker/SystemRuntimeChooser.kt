// Copyright 2020-2024, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

package org.khronos.openxr.system_runtime_broker

import android.content.Context
import org.khronos.openxr.broker_lib.OpenXRLoaderUtils
import org.khronos.openxr.runtime_broker.utils.RuntimeChooser
import org.khronos.openxr.runtime_broker.utils.RuntimeData

class SystemRuntimeChooser : RuntimeChooser {
    /**
     * Get the currently active runtime for a given specification major version.
     *
     * @param context a Context to use when searching for runtimes.
     * @param majorVersion a major version number of OpenXR.
     * @param abi the ABI to return data for.
     * @return the active runtime, or null if something went wrong or none were found.
     */
    override fun getActiveRuntime(context: Context, majorVersion: Int, abi: String): RuntimeData? {
        // OEMs should implement whatever is needed to access their runtime here,
        // and return a populated RuntimeData object.

        val runtimes = OpenXRLoaderUtils.findOpenXRRuntimes(context, majorVersion, abi)

        // For example:

        // single possible runtime package name
        // return runtimes?.firstOrNull { runtime -> runtime.packageName == apk }

        // Multiple possible runtime package names, in order of preference
        return runtimes?.firstOrNull { runtime -> packages.contains(runtime.packageName) }
    }

    companion object {
        // single possible runtime package name
        val apk = "org.freedesktop.monado.openxr_runtime.out_of_process"

        // Multiple possible runtime package names, in order of preference
        val packages =
            listOf(
                "org.freedesktop.monado.openxr_runtime.out_of_process",
                "org.freedesktop.monado.openxr_runtime.in_process"
            )
    }
}
