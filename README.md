# OpenXR™ Runtime Broker for Android™

<!--
Copyright 2022, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

This repository contains a number of pieces of standard middleware that enable
OpenXR™ applications on Android™. This project is maintained by the
[OpenXR Working Group][openxr] (part of the [Khronos® Group, Inc.][khronos]),
which is responsible for the OpenXR standard. It is co-maintained under the
umbrella of the [Monado][] project here at
<https://gitlab.freedesktop.org/monado/utilities/openxr-android-broker>.

[openxr]:https://khronos.org/openxr
[khronos]:https://khronos.org
[monado]:https://monado.freedesktop.org

The role and behavior of the projects in this repo is more formally described in
the [OpenXR Loader Design Document][loaderspec]. In particular see the section
on [Android Active Runtime Location][android-active-runtime-location] for an
overview of the process the standard cross-vendor loader uses to find an active
runtime.

[loaderspec]:https://registry.khronos.org/OpenXR/specs/1.0/loader.html
[android-active-runtime-location]:https://registry.khronos.org/OpenXR/specs/1.0/loader.html#android-active-runtime-location

The following projects are included in this repository:

## "Installable" Runtime Broker - `installable_runtime_broker/`

This is an Android application that is meant to be **released only by the
working group**. It is or will be published to app markets, as well as released
as a signed APK. Its user-facing purpose is to allow users to select and
authorize an OpenXR runtime that did not ship with their device (for plug-in
viewers, slide-in headsets, and similar). The OpenXR Loader contacts a Content
Provider exposed by this application to retrieve active runtime information.

This "installable" broker **must not** ship on phones or other Android-powered
devices unless it is able to be upgraded seamlessly from a market. Further,
**other applications must not** expose a content provider with the authority
`org.khronos.openxr.runtime_broker`. Violating either of these prevents the user
from following the normal flow for using or choosing runtimes, and will break
other OpenXR runtimes.

If you are making an OpenXR runtime that is "installable" and thus needs this
broker, part of your first-launch/out-of-box experience might include the
following steps:

- Guide the user to install this "installable" runtime broker, if it is not
  already installed. You can find it on Google Play™ or other market, or you may
  ship and install the APK signed by the Working Group. The canonical package
  name is `org.khronos.openxr.runtime_broker`.
- Suggest that the user enable your runtime as the active runtime, by launching
  the `.MainActivity` which will bring up the list of available runtimes. This
  activity can be treated as effectively a preference/settings activity,
  logically owned by the OpenXR Working Group rather than by the Android
  platform.

The installable broker finds available runtimes using metadata in the APK
manifest: see the loader design doc section
[Android Runtime Metadata for Installable Broker][android-runtime-metadata] for
details. You **must not** modify the broker to find your runtime: if you need a
different runtime location protocol, see the section about the System Broker
below.

See also [PRIVACY](PRIVACY.md), a document which is written primarily for users
of the "installable" OpenXR Runtime Broker.

See the [API Adopter Program][adopter-program] document on the Khronos web site
(and the Conformance Process Document and Trademark Guidelines it links to) for
details on conformance requirements for advertising your package as being an
OpenXR runtime. Current conformance status may be found at
<https://khronos.org/conformance>.

[android-runtime-metadata]:https://registry.khronos.org/OpenXR/specs/1.0/loader.html#android-runtime-metadata
[adopter-program]: https://www.khronos.org/conformance/adopters/

Note: Signed APKs and most store-installable versions of this package use a
different activity icon that contains the OpenXR logo trademark, used in
accordance with the trademark guidelines. The logo image itself has unclear
copyright licensing terms so is excluded from this open-source repository. See
the official [Khronos Trademark Guidelines][trademark] and
[Khronos Logos][logos] pages for more details.

[trademark]: https://www.khronos.org/legal/khronos-trademark-guidelines
[logos]: https://www.khronos.org/legal/trademarks/

## "System" Runtime Broker stub/template - `system_runtime_broker/`

If you are shipping an Android powered device that includes its own OpenXR
runtime, you may ship a "system" runtime broker that does not conflict with the
installable broker described above. The `system_runtime_broker` directory
contains the start of such a package, exposing a similar but distinct content
provider authority, `org.khronos.openxr.system_runtime_broker`. Unlike the
installable broker, the system broker is defined to be protected by a
"normal"-type permission, `org.khronos.openxr.permission.OPENXR_SYSTEM`, which
is automatically requested by apps that use the standard OpenXR loader `.aar`
package.

Please copy/fork the contents of this directory, as well as shared utility
libraries also found in this repo, and modify it so that it returns the relevant
data for your runtime when it is available. You may ship the resulting package
in your system image, but it should not be available for install on other
devices.

See the [API Adopter Program][adopter-program] document on the Khronos web site
(and the Conformance Process Document and Trademark Guidelines it links to) for
details on requirements for advertising your package as being an OpenXR runtime.

## Test applications: `sampleclient/`, `nativesampleclient/`

These two apps are for experimentation and testing purposes only, and
demonstrate the technique that the cross-vendor OpenXR loader uses to access the
runtime broker(s) from JVM and native code, respectively. They are not meant for
end-user usage.

## Helper libraries: `brokerlib/`, `utils/`, `vendor/`

`brokerlib/` contains generic and utility code used both in implementing the
"installable" runtime broker, as well as in the stub implementation of the
"system" runtime broker.

`utils/` contains utility code used both by the brokers as well as by the
`sampleclient`.

`vendor/` contains open-source library native code that is maintained elsewhere:

- `vendor/android-jni-wrappers`:
  <https://gitlab.freedesktop.org/monado/utilities/android-jni-wrappers>
- `vendor/jnipp`: Files in this repo are from the friendly fork at
  <https://github.com/rpavlik/jnipp>, upstream is at
  <https://github.com/mitchdowd/jnipp>

## License

This project follows the [REUSE][] specification, which means that in general
each file must carry a machine readable copyright notice and SPDX license
identifier. (If this cannot be embedded in a file, it is adjacent in a file with
`.license` appended to the name, or in the repository-wide `.reuse/dep5`.) The
copyright and license data in a given file is authoritative in case of any
doubts or conflicts. However, in general:

- Code written expressly for this repository is licensed under the Boost
  Software License version 1.0 (BSL-1.0). This includes most files in:
  - `broker_lib/`
  - `installable_runtime_broker/`
  - `nativesampleclient/`
  - `sampleclient/`
  - `system_runtime_broker/`
  - `utils/`
- Small config-type files with minimal creative content (such as `.gitignore`
  files) are CC0-1.0.
- Text documents such as this README are CC-BY-4.0.
- Some assets are derived from Apache-2.0 sources and so remain under
  Apache-2.0.
- Software from other projects remains under its original license (MIT for
  `vendor/jnipp`, Apache-2.0 for the Gradle Wrapper, etc.)
- Gradle downloads third-party dependencies in the course of its build, which
  are attributed with their licenses (mostly Apache-2.0) in the UI of the
  installable runtime broker.

## Contributing

Contribution is welcome and encouraged! Submitted changes must be licensed under
the same license applicable to other content of a similar type already in the
repository - typically this means BSL-1.0. All submissions must continue to pass
`reuse lint` by carrying an appropriate copyright and license annotation, which
must be accurate.

[REUSE]:https://reuse.software

## Trademarks

OpenXR™ and the OpenXR logo are trademarks owned by The Khronos Group Inc. and
are registered as a trademark in China, the European Union, Japan and the United
Kingdom.

Khronos and the Khronos Group logo are registered trademarks of the Khronos
Group Inc.

Android is a trademark of Google LLC.

Google Play and the Google Play logo are trademarks of Google LLC.

All other product names, trademarks, and/or company names are used solely for
identification and belong to their respective owners.
