# SPDX-FileCopyrightText: 2024, Collabora, Ltd.
# SPDX-License-Identifier: CC0-1.0

# Generate build/outputs/mapping/... files for retracing
-keepattributes LineNumberTable,SourceFile
-renamesourcefileattribute SourceFile
